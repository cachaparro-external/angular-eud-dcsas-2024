import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormExample1Component } from './reactive-form-example-1/reactive-form-example-1.component';
import { ReactiveFormExample2Component } from './reactive-form-example-2/reactive-form-example-2.component';
import { ReactiveFormExample3Component } from './reactive-form-example-3/reactive-form-example-3.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { IfExampleComponent } from './if-example/if-example.component';
import { SwitchExampleComponent } from './switch-example/switch-example.component';
import { RouterParamsComponent } from './router-params/router-params.component';
import { RouterRedirectComponent } from './router-redirect/router-redirect.component';
import { CheckRoleGuard } from './util/guard/CheckRoleGuard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'optionA'
  },
  {
    path: 'optionA',
    component: ReactiveFormExample1Component
  },
  {
    path: 'optionB',
    component: ReactiveFormExample2Component
  },
  {
    path: 'optionC',
    component: ReactiveFormExample3Component
  },
  {
    path: 'optionD',
    children: [
      {
        path: "select",
        component: IfExampleComponent,
        title: "Consulta"
      },
      {
        path: "create",
        component: SwitchExampleComponent,
        title: "Creación"
      }
    ]
  },
  //Path
  /*{
    path: 'optionE/:tableName',
    component: RouterParamsComponent
  },*/
  //Query
  {
    path: 'optionE',
    component: RouterParamsComponent
  },
  {
    path: 'redirect',
    component: RouterRedirectComponent
  },
  {
    path: 'redirect2',
    component: RouterRedirectComponent,
    canActivate: [CheckRoleGuard]
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
