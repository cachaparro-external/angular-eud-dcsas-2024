import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyComponentComponent } from './my-component/my-component.component';
import { Form1Component } from './form1/form1.component';
import { OuterComponent } from './outer/outer.component';
import { InnerComponent } from './inner/inner.component';
import { Inner2Component } from './inner2/inner2.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { IfExampleComponent } from './if-example/if-example.component';
import { HiddenExampleComponent } from './hidden-example/hidden-example.component';
import { If2ExampleComponent } from './if2-example/if2-example.component';
import { ForExampleComponent } from './for-example/for-example.component';
import { SwitchExampleComponent } from './switch-example/switch-example.component';
import { DirectiveAttrExampleComponent } from './directive-attr-example/directive-attr-example.component';
import { PipesExampleComponent } from './pipes-example/pipes-example.component';
import { PowPipe } from './pipes/pow.pipe';
import { ReactiveFormExample1Component } from './reactive-form-example-1/reactive-form-example-1.component';
import { ReactiveFormExample2Component } from './reactive-form-example-2/reactive-form-example-2.component';
import { ReactiveFormExample3Component } from './reactive-form-example-3/reactive-form-example-3.component';
import { HttpClientExampleComponent } from './http-client-example/http-client-example.component';
import { TokenBearerInterceptor } from './util/interceptor/TokenBearerInterceptor';
import { HttpResponseInterceptor } from './util/interceptor/HttpResponseInterceptor';
import { MenuComponent } from './menu/menu.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { RouterParamsComponent } from './router-params/router-params.component';
import { RouterRedirectComponent } from './router-redirect/router-redirect.component';

@NgModule({
  declarations: [
    AppComponent,
    MyComponentComponent,
    Form1Component,
    OuterComponent,
    InnerComponent,
    Inner2Component,
    IfExampleComponent,
    HiddenExampleComponent,
    If2ExampleComponent,
    ForExampleComponent,
    SwitchExampleComponent,
    DirectiveAttrExampleComponent,
    PipesExampleComponent,
    PowPipe,
    ReactiveFormExample1Component,
    ReactiveFormExample2Component,
    ReactiveFormExample3Component,
    HttpClientExampleComponent,
    MenuComponent,
    NotFoundComponent,
    RouterParamsComponent,
    RouterRedirectComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenBearerInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpResponseInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
