import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserDTO } from '../../types/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private MAIN_URL : string = "https://gorest.co.in/public/v2/users";
  private TOKEN : string = "759da2c315f1ab0c74bd630e992d3a67405dc44fc749bfe973d59bd0bc2763e4";

  constructor(private httpClient : HttpClient) {

  }

  //Solo Payload
  getAll() : Observable<Array<UserDTO>>{
    return this.httpClient.get<Array<UserDTO>>(this.MAIN_URL);
  }

  getAll2(callback : any) : void{
    this.httpClient.get<Array<UserDTO>>(this.MAIN_URL)
      .subscribe({
        next: (response) => {
          callback(response)
        },
        error: (error) => {
          console.error(error);
          callback([]);
        }
      });
  }

  //Todo el objetode respuesta HTTP
  getAll3() : Observable<HttpResponse<Array<UserDTO>>>{
    return this.httpClient.get<Array<UserDTO>>(this.MAIN_URL, {
      observe: 'response'
    });
  }

  getById(id : number) : Observable<UserDTO>{
    let url = `${this.MAIN_URL}/${id}`;
    //let url = this.MAIN_URL + "/" +  id;

    return this.httpClient.get<UserDTO>(url);
  }

  getAllPag(page : number, perPage : number) : Observable<UserDTO[]>{
    const params : HttpParams = new HttpParams()
    .set("page", page)
    .set("per_page", perPage);

    const options = {
      params: params
    }

    return this.httpClient.get<UserDTO[]>(this.MAIN_URL, options);

    /*return this.httpClient.get<UserDTO[]>(this.MAIN_URL,
      {
        params: params
      });*/
  }

  create(user : UserDTO) : Observable<HttpResponse<UserDTO>>{
    const headersVar : HttpHeaders = new HttpHeaders()
      .append("Accept", "application/json")
      .append("Content-type", "application/json")
      //.append("Authorization", "Bearer " + this.TOKEN)
      .append("My-header", "My-Value");

    user.status = 'active';

    return this.httpClient.post<UserDTO>(this.MAIN_URL, user,
      {
        headers: headersVar,
        observe: 'response'
      });
  }
}
