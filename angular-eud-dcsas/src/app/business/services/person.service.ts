import { Injectable } from '@angular/core';
import { Person } from '../types/Person';
import { HttpClient } from '@angular/common/http';
import { UserDTO } from '../types/User';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  people : Person[];

  constructor(private http : HttpClient) {
    this.people = [
      {
        firstName : "Luisa",
        lastName: "Rodriguez",
        age : 30
      },
      {
        firstName : "Alberto",
        lastName: "Rios",
        age : 25,
        employee: true
      },
      {
        firstName : "Maria",
        lastName: "Perez",
        age : 50,
        employee: false
      }
    ]
  }

  public getPeople() : Person[]{
    return this.people;
  }

  //users : Array<UserDTO> | null = null;

  /*public algo (){
    Observable<Array<UserDTO>>> obs = this.http.get<Array<UserDTO>>("");
    obs.subscribe({
      next: (respoonse) => {
        this.users = respoonse;
      }

    })

    return Promise.resolve(this.users);
  }*/
}
