export type Person = {
  firstName : string,
  lastName : string,
  age : number,
  employee? : boolean
}
