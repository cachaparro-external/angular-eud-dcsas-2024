export type UserDTO = {
  id?: number,
  name : string,
  email: string,
  gender : string,
  status?: string
}
