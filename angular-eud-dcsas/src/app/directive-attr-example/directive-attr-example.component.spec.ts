import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectiveAttrExampleComponent } from './directive-attr-example.component';

describe('DirectiveAttrExampleComponent', () => {
  let component: DirectiveAttrExampleComponent;
  let fixture: ComponentFixture<DirectiveAttrExampleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DirectiveAttrExampleComponent]
    });
    fixture = TestBed.createComponent(DirectiveAttrExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
