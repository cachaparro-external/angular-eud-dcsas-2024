import { Component } from '@angular/core';

@Component({
  selector: 'app-directive-attr-example',
  templateUrl: './directive-attr-example.component.html',
  styleUrls: ['./directive-attr-example.component.css']
})
export class DirectiveAttrExampleComponent {

  selectClass : boolean = true;
  classList : string[] = [];

  selectStyle : boolean = true;
  styleList : Record<string, string> = {};

  size1 : string = "25px";
  size2 : string = "25";

  changeClass() : void{
    this.selectClass = !this.selectClass;

    if(this.selectClass){
      this.classList = ['style2', 'style3'];
    }else{
      this.classList = ['style1'];
    }
  }

  changeStyle() : void {
    this.selectStyle = !this.selectStyle;

    if(this.selectStyle){
      this.styleList = {
        'background-color': 'yellow',
        color: 'black'
      }
    }else{
      this.styleList = {
        'background-color': 'blue',
        color: 'white'
      }
    }
  }

}
