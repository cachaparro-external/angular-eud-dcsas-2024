import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForExampleComponent } from './for-example.component';

describe('ForExampleComponent', () => {
  let component: ForExampleComponent;
  let fixture: ComponentFixture<ForExampleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ForExampleComponent]
    });
    fixture = TestBed.createComponent(ForExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
