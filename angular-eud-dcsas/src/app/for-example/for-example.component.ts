import { Component, OnInit } from '@angular/core';
import { Person } from '../business/types/Person';
import { PersonService } from '../business/services/person.service';

@Component({
  selector: 'app-for-example',
  templateUrl: './for-example.component.html',
  styleUrls: ['./for-example.component.css']
})
export class ForExampleComponent implements OnInit{

  people : Person[];
  show : boolean = false;

  constructor(private personService : PersonService){
    this.people = [];
  }

  ngOnInit(): void {
    this.people = this.personService.getPeople();
  }

  selectPeople() : void{
    this.show = true;
  }

}
