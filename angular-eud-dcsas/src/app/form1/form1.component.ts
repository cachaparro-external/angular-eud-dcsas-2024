import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-form1',
  templateUrl: './form1.component.html',
  styleUrls: ['./form1.component.css']
})
export class Form1Component implements OnInit, OnDestroy{

  email : string = "";
  password : string = "";
  checked : string = "false";

  disableSubmit : boolean = true;
  hiddenSubmit : boolean = false;
  name : string = "";

  constructor(){
    this.name = "No definido";
    console.log("Entro a constructor");
  }

  ngOnInit(): void {
    console.log("Entro a ngOnInit");
  }

  clickHidden() : void{
    this.hiddenSubmit = !this.hiddenSubmit;

    if(this.hiddenSubmit){
      this.name = "Laura";
    }else{
      this.name = "Mario";
    }
  }

  fillInfo() : void{
    this.email = "test@test.com";
    this.password = "YYY";
    this.checked = "true";
  }

  ngOnDestroy(): void {
    console.log("Entro a ngOnDestroy");
  }

}
