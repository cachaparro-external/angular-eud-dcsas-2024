import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HiddenExampleComponent } from './hidden-example.component';

describe('HiddenExampleComponent', () => {
  let component: HiddenExampleComponent;
  let fixture: ComponentFixture<HiddenExampleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HiddenExampleComponent]
    });
    fixture = TestBed.createComponent(HiddenExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
