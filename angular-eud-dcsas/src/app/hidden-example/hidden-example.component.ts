import { Component } from '@angular/core';

@Component({
  selector: 'app-hidden-example',
  templateUrl: './hidden-example.component.html',
  styleUrls: ['./hidden-example.component.css']
})
export class HiddenExampleComponent {

  nombre : string = "";
  apellido : string = "";
  edad : number = 0;

  show : boolean = false;

  constructor(){
    this.nombre = "Monica";
    this.apellido = "Perez";
    this.edad = 25;
  }

  changeShow() : void {
    this.show = !this.show;
  }

}
