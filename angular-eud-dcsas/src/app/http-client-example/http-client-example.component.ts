import { UserDTO } from './../business/types/User';
import { Component } from '@angular/core';
import { UserService } from '../business/services/client/user.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-http-client-example',
  templateUrl: './http-client-example.component.html',
  styleUrls: ['./http-client-example.component.css']
})
export class HttpClientExampleComponent {

  constructor(private userService : UserService){

  }
  /*execute() : void {
    this.userService.getAll()
      .subscribe({
        next: (response) => {
          //2XX
          console.log(response);
        },
        error: (error) => {
          //4XX o 5XX
          console.error(error);
        }
      })
  }*/

  /*private callbackUsers = function(users : Array<UserDTO>){
        console.log(users);
  };

  execute() : void{
    this.userService.getAll2(this.callbackUsers);
  }*/
  execute() : void {
    this.userService.getAll3()
      .subscribe({
        next: (response : HttpResponse<Array<UserDTO>>) => {
          //2XX
          console.log(response);

          if(response.status === 200){
            console.log("Body: " + response.body);
            console.log("Headers: " + response.headers);
            console.log("HTTP Code: " + response.status);
          }else{
            console.log("Código diferente a 200");
          }
        },
        error: (error : HttpErrorResponse) => {
          //4XX o 5XX
          console.log(error);
          console.log("Error: " + error.error);
          console.log("Headers: " + error.headers);
          console.log("HTTP Code: " + error.status);
        }
      })
  }

  executeById() : void{
    this.userService.getById(6940254)
      .subscribe({
        next: (data) => {
          console.log(data);
        },
        error: (error : HttpErrorResponse) => {
          //4XX o 5XX
          console.log("Error: " + JSON.stringify(error.error));
          console.log("Headers: " + JSON.stringify(error.headers));
          console.log("HTTP Code: " + error.status);
        },
        complete: () => {
          console.log("Terminando ejecución");
        }
      });
  }

  executeAllPag() : void{
    this.userService.getAllPag(2, 5)
      .subscribe({
        next: (data) => {
          console.log(data);
        },
        error: (error : HttpErrorResponse) => {
          //4XX o 5XX
          console.log("Error: " + JSON.stringify(error.error));
          console.log("Headers: " + JSON.stringify(error.headers));
          console.log("HTTP Code: " + error.status);
        },
        complete: () => {
          console.log("Terminando ejecución");
        }
      });
  }

  executeCreate() : void{
    const user : UserDTO = {
      name : "Raul Garcia",
      email: "rgarcia@test.com",
      gender: "male"
    };

    this.userService.create(user)
      .subscribe({
        next: (response : HttpResponse<UserDTO>) => {
          if(response.status === 201){
            console.log("Se creo el usuario con ID " + response.body?.id);
          }else{
            console.error("Se retornor el HTTP Code " + response.status);
          }

          console.log("Content-Type: ", response.headers.get('Content-Type'));
          console.log("Location: ", response.headers.get('location'));
        },
        error: (error : HttpErrorResponse) => {
          if(error.status === 422){
            console.log("Petición incorrecta o usuario ya existe")
          }else{
            console.log(error);
          }
        }
      })
  }
}
