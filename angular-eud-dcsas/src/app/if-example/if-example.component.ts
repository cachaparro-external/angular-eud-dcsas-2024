import { Component } from '@angular/core';

@Component({
  selector: 'app-if-example',
  templateUrl: './if-example.component.html',
  styleUrls: ['./if-example.component.css']
})
export class IfExampleComponent {

  nombre : string = "";
  apellido : string = "";
  edad : number = 0;

  show : boolean = false;

  constructor(){
    this.nombre = "Monica";
    this.apellido = "Perez";
    this.edad = 25;
  }

  changeShow() : void {
    this.show = !this.show;
  }
}
