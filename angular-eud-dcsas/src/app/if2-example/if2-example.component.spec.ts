import { ComponentFixture, TestBed } from '@angular/core/testing';

import { If2ExampleComponent } from './if2-example.component';

describe('If2ExampleComponent', () => {
  let component: If2ExampleComponent;
  let fixture: ComponentFixture<If2ExampleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [If2ExampleComponent]
    });
    fixture = TestBed.createComponent(If2ExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
