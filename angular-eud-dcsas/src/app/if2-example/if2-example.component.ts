import { Component } from '@angular/core';

@Component({
  selector: 'app-if2-example',
  templateUrl: './if2-example.component.html',
  styleUrls: ['./if2-example.component.css']
})
export class If2ExampleComponent {

  nombre : string = "";
  apellido : string = "";
  edad : number = 0;

  show : boolean = false;

  constructor(){
    this.nombre = "Monica";
    this.apellido = "Perez";
    this.edad = 25;
  }

  changeShow() : void {
    this.show = !this.show;
  }
}
