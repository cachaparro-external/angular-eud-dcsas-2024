import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-inner',
  templateUrl: './inner.component.html',
  styleUrls: ['./inner.component.css']
})
export class InnerComponent {

  @Input()
  nombres : string = "";
  @Input()
  apellidos : string = "";
  countFN : number = 0;
  countLN : number = 0;

  @Output()
  isStudent : EventEmitter<boolean>;

  isStudentBoolean : boolean = false;

  constructor(){
    this.isStudent = new EventEmitter<boolean>();
  }

  countChars() : void{
    this.countFN = this.nombres.length;
    this.countLN = this.apellidos.length;
  }

  checkNotEmpty() : boolean{
    return !(this.nombres.length > 0 && this.apellidos.length > 0);
  }

  changeIsStudent() : void{
    this.isStudentBoolean = !this.isStudentBoolean;
    this.isStudent.emit(this.isStudentBoolean);
  }
}
