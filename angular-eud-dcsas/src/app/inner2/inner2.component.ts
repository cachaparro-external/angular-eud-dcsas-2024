import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-inner2',
  templateUrl: './inner2.component.html',
  styleUrls: ['./inner2.component.css']
})
export class Inner2Component {

  private _nombres : string = "";
  private _apellidos : string = "";
  countFN : number = 0;
  countLN : number = 0;

  countChars() : void{
    if(this.checkNotEmpty()){
      this.countFN = this.nombres.length;
      this.countLN = this.apellidos.length;
    }
  }

  checkNotEmpty() : boolean{
    return this.nombres.length > 0 && this.apellidos.length > 0;
  }

  @Input()
  get nombres() : string{
    return this._nombres;
  }

  set nombres(nombres : string){
    this._nombres = nombres;
    this.countChars();
  }

  @Input()
  get apellidos() : string{
    return this._apellidos;
  }

  set apellidos(apellidos: string){
    this._apellidos = apellidos;
    this.countChars();
  }

}
