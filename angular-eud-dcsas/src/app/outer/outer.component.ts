import { Component } from '@angular/core';

@Component({
  selector: 'app-outer',
  templateUrl: './outer.component.html',
  styleUrls: ['./outer.component.css']
})
export class OuterComponent {

  firtsName : string;
  lastName : string;
  age : number;
  employee : boolean;
  student : boolean;

  constructor(){
    this.firtsName = "";
    this.lastName = "";
    this.age = 0;
    this.employee = false;
    this.student = false;
  }

  fillData() : void {
    this.firtsName = "Natalia";
    this.lastName = "Vega";
    this.age = 20;
    this.employee = true;
  }

  setIsStudent(isStudent : boolean) : void{
    this.student = isStudent;
  }
}
