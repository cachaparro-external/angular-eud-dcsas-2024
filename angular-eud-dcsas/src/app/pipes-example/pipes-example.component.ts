import { Component } from '@angular/core';

@Component({
  selector: 'app-pipes-example',
  templateUrl: './pipes-example.component.html',
  styleUrls: ['./pipes-example.component.css']
})
export class PipesExampleComponent {

  cadena : string = "ABCcde123$%";
  cantidad : number = 45.1234;
  fechaActual : Date = new Date();

  formatoFecha : string = 'short';

  estructura : any = {
    name: "Carlos",
    lastName: "Vega",
    age: 25
  }

}
