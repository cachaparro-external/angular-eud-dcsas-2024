import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pow'
})
export class PowPipe implements PipeTransform {

  transform(value: number, ...args: string[]): number {
    //args[0]  --> '2'
    return value * value;
  }

}
