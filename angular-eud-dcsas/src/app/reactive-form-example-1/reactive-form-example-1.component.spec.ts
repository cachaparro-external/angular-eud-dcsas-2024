import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactiveFormExample1Component } from './reactive-form-example-1.component';

describe('ReactiveFormExample1Component', () => {
  let component: ReactiveFormExample1Component;
  let fixture: ComponentFixture<ReactiveFormExample1Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ReactiveFormExample1Component]
    });
    fixture = TestBed.createComponent(ReactiveFormExample1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
