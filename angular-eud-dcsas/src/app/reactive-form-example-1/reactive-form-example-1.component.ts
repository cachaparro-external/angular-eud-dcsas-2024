import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactive-form-example-1',
  templateUrl: './reactive-form-example-1.component.html',
  styleUrls: ['./reactive-form-example-1.component.css']
})
export class ReactiveFormExample1Component {

  email : FormControl;
  password : FormControl;
  check : FormControl;

  constructor(){
    this.email = new FormControl("algo@test.com", [Validators.required, Validators.minLength(3)]);
    this.password = new FormControl({value: "", disabled: true});
    this.check = new FormControl(true);
  }

  submit() : void{
    console.log("Email: " + this.email.value);
    console.log("Email Valid: " + this.email.valid);
    console.log("Password: " + this.password.value);
    console.log("Check: " + this.check.value);
  }

}
