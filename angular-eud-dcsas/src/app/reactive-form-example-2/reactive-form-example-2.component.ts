import { Component } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactive-form-example-2',
  templateUrl: './reactive-form-example-2.component.html',
  styleUrls: ['./reactive-form-example-2.component.css']
})
export class ReactiveFormExample2Component {

  myForm : FormGroup;
  myFormArray : FormArray;
  /*email : FormControl;
  password : FormControl;
  check : FormControl;*/

  constructor(){
    /*this.email = new FormControl("algo@test.com");
    this.password = new FormControl({value: "", disabled: true});
    this.check = new FormControl(true);*/

    /*this.myForm = new FormGroup({
      email : this.email,
      password : this.password,
      check: this.check
    });*/

    this.myForm = new FormGroup({
      email: new FormControl("", [Validators.maxLength(10)]),
      password: new FormControl(""),
      check: new FormControl(true)
    });

    this.myFormArray = new FormArray([
      new FormControl(""),
      new FormControl(""),
      new FormControl(true)
    ]);
  }

  onSubmit() : void{
    /*console.log("Email: " + this.email.value);
    console.log("Email Valid: " + this.email.valid);
    console.log("Password: " + this.password.value);
    console.log("Check: " + this.check.value);*/

    console.log("Email: " + this.myForm.value.email);
    console.log("Email Valid: " + this.myForm.get('email')?.valid);
    console.log("Form Valid: " + this.myForm.valid);
    console.log("Password: " + this.myForm.value.password);
    console.log("Check: " + this.myForm.value.check);
  }

}
