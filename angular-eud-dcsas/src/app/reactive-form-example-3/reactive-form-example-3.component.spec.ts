import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactiveFormExample3Component } from './reactive-form-example-3.component';

describe('ReactiveFormExample3Component', () => {
  let component: ReactiveFormExample3Component;
  let fixture: ComponentFixture<ReactiveFormExample3Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ReactiveFormExample3Component]
    });
    fixture = TestBed.createComponent(ReactiveFormExample3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
