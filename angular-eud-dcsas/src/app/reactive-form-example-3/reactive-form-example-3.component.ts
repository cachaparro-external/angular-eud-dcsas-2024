import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactive-form-example-3',
  templateUrl: './reactive-form-example-3.component.html',
  styleUrls: ['./reactive-form-example-3.component.css']
})
export class ReactiveFormExample3Component {

  myForm : FormGroup;
  email : FormControl;
  check : FormControl;

  constructor(private builder : FormBuilder){
    this.email = this.builder.control("", [Validators.required, Validators.minLength(3), Validators.email]);
    this.check = this.builder.control(false, [], []);

    this.myForm = this.builder.group({
      //email: ["", [Validators.required, Validators.minLength(3), Validators.email], []],
      email: this.email,
      password: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(10)], []],
      check: this.check
    });

    //this.resetForm();

    //Cambio a nivel de los valores del formulario
    /*this.myForm.valueChanges.subscribe((newValue) => {
      console.log("Form valueChanges: " + JSON.stringify(newValue));
    });*/

    //Cambio a nivel de los vales del Check
    /*this.check.valueChanges.subscribe((newValue) => {
      console.log("Check valueChanges: " + newValue);
    });*/

    //Cambio a nivel del estado del formulario (valid -> invalid, invalid -> valid)
    /*this.myForm.statusChanges.subscribe((newStatus => {
      console.log("Form statusChanges: " + newStatus);
    }));*/

    this.email.valueChanges.subscribe((newValue => {
      console.log("Email valueChanges: " + newValue);
    }));

    this.email.statusChanges.subscribe((newStatus => {
      console.log("Email statusChanges: " + newStatus);
    }));
  }

  onSubmit() : void{
    console.log("Email: " + this.myForm.value.email);
    console.log("Email Valid: " + this.myForm.get('email')?.valid);
    console.log("Form Valid: " + this.myForm.invalid);
    console.log("Password: " + this.myForm.value.password);
    console.log("Check: " + this.check.valid);

    console.log("Email error ", this.myForm.get('email')?.errors);
    console.log("Email error " +  JSON.stringify(this.myForm.get('email')?.errors)) ;

    //this.myForm.value.email = "ing@test.com";

    /*this.myForm.setValue({
      email: "test@test.com",
      password: "******",
      check: true
    });*/

    //this.email.setValue("ing@test.com");

    this.myForm.patchValue({
      email: "test@test.com",
      password: "******"
    });
  }

  resetForm() : void{
    this.myForm.setValue({
      email: "test@test.com",
      password: "",
      check: true
    })
  }

}
