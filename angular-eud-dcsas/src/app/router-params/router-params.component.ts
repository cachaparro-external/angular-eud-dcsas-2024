import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { switchMap, Observable } from 'rxjs';

@Component({
  selector: 'app-router-params',
  templateUrl: './router-params.component.html',
  styleUrls: ['./router-params.component.css']
})
export class RouterParamsComponent implements OnInit{

  tableName : string | null = null;

  constructor(private activeView : ActivatedRoute
  ){

  }
  ngOnInit(): void {
    //mipagina/25
    /*this.activeView.params.subscribe(params => {
      this.tableName = params['tableName']
    });*/

    //mipagina?id=25
    this.activeView.queryParams.subscribe(params => {
      this.tableName = params['tableName']
    });
  }

}
