import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterRedirectComponent } from './router-redirect.component';

describe('RouterRedirectComponent', () => {
  let component: RouterRedirectComponent;
  let fixture: ComponentFixture<RouterRedirectComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RouterRedirectComponent]
    });
    fixture = TestBed.createComponent(RouterRedirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
