import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-router-redirect',
  templateUrl: './router-redirect.component.html',
  styleUrls: ['./router-redirect.component.css']
})
export class RouterRedirectComponent {

  tableName : string = "PERSONAS";

  constructor(private router : Router,
    private currentView : ActivatedRoute
  ){

  }

  public executeRedirect() : void{
    //optionA
    //this.router.navigate(['/optionA']);
    //this.router.navigateByUrl('/optionA');

    //PATH
    ///optionE/:tableName
    //this.router.navigate(['/optionE', this.tableName]);
    //this.router.navigateByUrl("/optionE/" + this.tableName);

    //Query
    ///optionE?tableName=PERSONAS
    //this.router.navigate(['/optionE'], {queryParams: {tableName: 'CIUDADES'}})
    //this.router.navigateByUrl("/optionE?tableName="  + this.tableName);

    this.router.navigate(["../example"], {relativeTo: this.currentView});
  }

}
