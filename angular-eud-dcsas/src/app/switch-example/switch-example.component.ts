import { Component } from '@angular/core';

@Component({
  selector: 'app-switch-example',
  templateUrl: './switch-example.component.html',
  styleUrls: ['./switch-example.component.css']
})
export class SwitchExampleComponent {

  nombre : string = "";
  apellido : string = "";
  edad : number = 0;
  profesion : string = "";

  show : boolean = false;

  constructor(){
    this.nombre = "Monica";
    this.apellido = "Perez";
    this.edad = 25;
  }

  changeShow() : void {
    this.show = !this.show;
  }

}
