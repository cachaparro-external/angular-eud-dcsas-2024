import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { filter, Observable, tap } from "rxjs";

@Injectable()
export class HttpResponseInterceptor implements HttpInterceptor{
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
      .pipe(
        filter(event => event instanceof HttpResponse),
        tap({
          next: (event) => {
            console.log("Interceptor HttpResponse: ", event);

            const newEvent : HttpResponse<any> = (event as HttpResponse<any>);

            console.log("Interceptor  Status: ", newEvent.status);

            if(newEvent.status === 401){
              //Regenerar de nuevo el token
            }
          },
          error: (error : HttpErrorResponse) => {
            console.error("Interceptor  Error: ", error);
          }
        })
      )
  }
}
