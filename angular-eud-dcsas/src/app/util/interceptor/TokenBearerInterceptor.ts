import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable()
export class TokenBearerInterceptor implements HttpInterceptor{

  private TOKEN : string = "759da2c315f1ab0c74bd630e992d3a67405dc44fc749bfe973d59bd0bc2763e4";
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(req.method !== 'GET'){
      console.log("Interceptando request: ", req);

      const modifiedReq = req.clone({
        headers: req.headers.append("Authorization", "Bearer ".concat(this.TOKEN))
      });

      console.log("Procesando request: ", modifiedReq);

      return next.handle(modifiedReq);
    }else{
      return next.handle(req);
    }
  }

}
