var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Animal1 = /** @class */ (function () {
    function Animal1() {
        this.name = "";
        this.age = 0;
        this.lentgh = 0;
    }
    Animal1.prototype.getName = function () {
        return this.name;
    };
    Animal1.prototype.comer = function () {
        console.log("Comer");
    };
    return Animal1;
}());
var Carnivoro = /** @class */ (function (_super) {
    __extends(Carnivoro, _super);
    function Carnivoro() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Carnivoro.prototype.comer = function () {
        this.lentgh;
        console.log("Comer carne");
    };
    Carnivoro.prototype.mover = function () {
        console.log("Moverse");
    };
    return Carnivoro;
}(Animal1));
var Herviboro = /** @class */ (function (_super) {
    __extends(Herviboro, _super);
    function Herviboro() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Herviboro.prototype.comer = function () {
        console.log("Comer verdura");
    };
    Herviboro.prototype.mover = function () {
        console.log("Moverse");
    };
    return Herviboro;
}(Animal1));
var animal = new Animal1();
animal.comer();
animal = new Carnivoro();
animal.comer();
animal.mover();
animal = new Herviboro();
animal.comer();
animal.mover();
