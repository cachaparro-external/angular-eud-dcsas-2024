class Animal1{

    private name : string;
    public age : number;
    protected lentgh : number;

    constructor(){
        this.name = "";
        this.age = 0;
        this.lentgh = 0;
    }

    public getName() : string{
        return this.name;
    }

    public comer() : void{
        console.log("Comer");
    }

}

class Carnivoro extends Animal1{

    public comer() : void{
        this.lentgh;
        console.log("Comer carne");
    }

    public mover() : void {
        console.log("Moverse");
    }

}

class Herviboro extends Animal1{
    public comer() : void{
        console.log("Comer verdura");
    }

    public mover() : void {
        console.log("Moverse");
    }

}

let animal : Animal1 = new Animal1();
animal.comer();

animal = new Carnivoro();
animal.comer();
(animal as Carnivoro).mover();

animal = new Herviboro();
animal.comer();
(animal as Herviboro).mover();
