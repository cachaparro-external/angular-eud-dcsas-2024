var TestClass = /** @class */ (function () {
    function TestClass() {
    }
    TestClass.prototype.hacerAlgo = function () {
        console.log("Hacer algo");
    };
    TestClass.prototype.print = function (name) {
        return "Hello " + name;
    };
    TestClass.prototype.despedir = function () {
        return "Adios";
    };
    return TestClass;
}());
var myClass = new TestClass();
myClass.hacerAlgo();
console.log(myClass.print("Maria"));
console.log(myClass.despedir());
var myInterface = new TestClass();
myInterface.hacerAlgo();
console.log(myInterface.print("Jose"));
