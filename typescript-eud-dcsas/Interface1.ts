interface TestInterface{
    hacerAlgo() : void;
    print(name : string) : string;
}

class TestClass implements TestInterface{
    
    hacerAlgo(): void {
        console.log("Hacer algo")
    }
    
    print(name: string): string {
        return "Hello " + name;
    }

    despedir() : string{
        return "Adios";
    }
    
}

let myClass : TestClass = new TestClass();
myClass.hacerAlgo();
console.log(myClass.print("Maria"));
console.log(myClass.despedir());

let myInterface : TestInterface = new TestClass();
myInterface.hacerAlgo();
console.log(myInterface.print("Jose"));

