interface Person{
    name : string,
    id : number,
    birhtDate : Date
}

interface Employee extends Person{
    job : string,
    hireDate: Date
}

interface Student extends Person{
    grade : string,
    semester?: number
}

let e1 : Employee = {
    name : "Cesar",
    id: 1,
    birhtDate: new Date(),
    job: "CEO",
    hireDate: new Date()
}

let es1 : Student = {
    name : "Camila",
    id: 2,
    birhtDate: new Date(),
    grade: "ING"
}

let per1 : Employee | Student;
let per2 : Employee & Student;