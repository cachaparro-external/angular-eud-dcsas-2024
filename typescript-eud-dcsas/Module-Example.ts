export interface MyInterface{
    name : string
}

export type MyType = {
    name : string
}

export class MyClass{
    name : string
}

export enum Status {
    ACTIVO, //0
    INACTIVO
}

export enum HTTP_CODE{
    OK = 200,
    NO_CONTENT = 204,
    ERROR = 500 // ERROR = 'Interla Error'
}