console.log("Iniciando programa");

async function myFunc(){
    console.log("Ejecutando myFunc")
    //return "Cadena de prueba";
    //return Promise.resolve("Cadena de prueba 2");
    
    return Promise.reject("Error");
    //throw new Error("Error");
}

myFunc()
    .then(result => {
        console.log("Return " + result);
    }).catch(error => {
        console.error("Error: " + error);
    });

console.log("Finalizando programa");