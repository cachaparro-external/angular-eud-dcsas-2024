console.log("Iniciando programa");

const delay = sec => new Promise(resolve => setTimeout(resolve, sec * 1000));

/*function delay(sec){
    return new new Promise(resolve => setTimeout(resolve, sec * 1000));
}*/

const myFunc = async () => {
    let wait = Math.ceil(Math.random() * 10);
    console.log("1 - Esperando " + wait +  " segundos");
    await delay(wait);

    wait = Math.ceil(Math.random() * 10);
    console.log("2 - Esperando " + wait +  " segundos");
    await delay(wait);

    wait = Math.ceil(Math.random() * 10);
    console.log("3 - Esperando " + wait +  " segundos");
    await delay(wait);
}

myFunc();

console.log("Finalizando programa");