class Persona{

    #otherField = 0;
    
    constructor(genero){
        this.genero = genero;
    }

    print(){
        console.log("Genero: " + this.genero);
    }

    #calcularEdad(){
        return 0;
    }
}

class Panadero extends Persona{

    constructor(name){
        super("M");
        this.name = name;
    }

    print(){
        super.print();
        console.log("Panadero: " + this.name);
    }
}

class Cerrajero extends Persona{

    constructor(name){
        super("F");
        this.name = name;
    }

    print(){
        super.print();
        console.log("Cerrajero: " + this.name);
    }
}

const persona = new Persona("M");
persona.print();
persona.genero = "M"

const persona1 = new Panadero("Camilo");
persona1.print();

//persona = persona1;
//persona.print();

export default Persona;