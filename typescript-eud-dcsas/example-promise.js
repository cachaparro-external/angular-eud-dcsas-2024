console.log("Iniciando programa");

const myPromise = new Promise(
    function(resolve, reject){
        let n = Math.floor(Math.random() * 10);

        console.log("Número generado: " + n);

        if(n <= 5){
            //Exitoso
            resolve(1);
        }else{
            //Fallido
            reject("El número no es menor que 5");
        }
    }    
);

/*myPromise
    .then(result => {
        console.log("Exito con resultado " + result);
    }).catch(error => {
        console.log("Error con resultado " + error);
    })*/

myPromise
    .then(result => {
        console.log("Exito con resultado " + result);
    }).catch(error => {
        console.log("Error con resultado " + error);
    }).finally(() => {
        console.log("Cerrando conexión a BD");
    });

console.log("Finalizando programa");