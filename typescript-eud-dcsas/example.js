//import {Persona} from "example-class.js"

cadena = 'Texto'
console.log("String:" + cadena)

cadena = 1
console.log("Number:" + cadena)

var texto = 'Texto'
console.log("Texto:" + texto)

var texto = 1
console.log("Number: " + texto)

let texto2 = 'Texto2'
console.log("Texto2:" + texto2)

texto2 = 1
console.log("Number: " + texto2)

const texto3 = 'Texto3'
console.log("Texto3: " + texto3)

//texto3 = 1
//console.log("Number: " + texto3)

let boleano = (texto3 == 1 ? true : false)

let arreglo1 = [1, 3, 5, 7]
let arreglo2 = [2, 4, 6, 8]

let arreglo = [...arreglo1, ...arreglo2]
//[1, 3, 5, 7, 2, 4, 6, 8]
console.log(arreglo);

let [num1, num2] = arreglo1;

console.log(num1);
console.log(num2);

//1 - Normal por defecto
function sumar(a, b){
    return a + b;
}

console.log("Suma1: " + sumar(1, 2));

//2 Anonima
let sumaVar = function(a, b){
    return a + b;
}

console.log("Suma2: " + sumaVar(2, 3));

//3 --> Arrow Function
let sumaArrowFunc = (a, b) => a + b;

console.log("Suma3: " + sumaArrowFunc(4, 5));

//const persona = new Persona("F");
//persona.print();


const array = [1, 2, 3, 4, 5, 1];
const mySet = new Set([1, 2, 3, 4, 5, 1]);
const myMap = new Map([["key1", "value1"], ["key2", "value2"]])
/*const myMap = [
    {
        "key1": "value1"
    },
    {
        "key2": "value2"
    }
]*/


//console.log(array);
//console.log(mySet);
console.log(myMap);
console.log(myMap.get("key1"));
myMap.set("key3", "value3");
console.log(myMap);

const names = ["Sol", "Luna", "Estrellas"];

for(let n of names){
    console.log(n);
}

/*for(let n of names){
    for(let i = 0; i < 5 ; i++){
        console.log(n + " - " + i);

        if(i == 2){
            //
        }
    }

    //console.log(i);
}*/

//console.log(n);

//const array2 = [1, 3, 5, 2, 5, 6, 7, 9];

//const arrayRes = array2.filter(num => num <= 5).map(num => num + 2);

//console.log(arrayRes);

/*function sumar(...numeros){
    let suma = 0;
    for(let n of numeros){
        suma += n;
    }
    return suma;
}

console.log(sumar(1));
console.log(sumar(1, 2));
console.log(sumar(1,2, 3, 4, 5));

function areaTriangulo(b, h = 1){
    return b + h / 2;
}

console.log(areaTriangulo(5));
console.log(areaTriangulo(5, 2));*/

let name = "Carlos"
let field = "edad"

let persona = {
    nombre: name,
    genero: "M"
}

let persona2 = {
    [field]: "Carlos",
    genero: "F"
}

//console.log(persona2)


let power = Math.pow(2, 3);
let power2 = 2 ** 3;

let myVar1 = undefined;//or null
let myVar2 = "Algun texto"

//console.log("1 - No Nulo " + (myVar1 ?? myVar2));
//console.log("2 - No Nulo " + (myVar2 ?? myVar1));

const empleado = {
    name: "Jose",
    age: 25,
    genero: "M",
    /*jobs:{
        name: "CEO",
        tiempo: 2
    }*/
}

console.log(empleado.name);
console.log(empleado.jobs?.tiempo);












