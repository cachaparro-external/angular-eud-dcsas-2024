var cadena = "Algún Texto";
var numero = 25;
var decimal = 5.2;
var valorVerdad = true;
var anyVar = "Cadena";
var PHI = 3.1416;
var array1;
var array2;
array1 = [1, 2, 3];
array2 = [4, 5];
console.log(cadena);
console.log(numero);
console.log(decimal);
console.log(valorVerdad);
console.log(cadena);
var soportaVarios;
soportaVarios = "Cadena";
console.log(soportaVarios);
soportaVarios = 1;
console.log(soportaVarios);
var p1 = {
    name: "Pedro",
    age: 30,
    birthDate: new Date()
};
var p2 = {
    name: "Pedro",
    age: 30,
    birthDate: new Date(),
    empleado: false
};
var Animal = /** @class */ (function () {
    function Animal() {
    }
    Animal.prototype.print = function (name) {
        console.log(name);
    };
    return Animal;
}());
var a = new Animal();
a.print("Name");
function sumar1(a, b) {
    return a + b;
}
function restar2(a, b) {
    console.log(a + b);
}
