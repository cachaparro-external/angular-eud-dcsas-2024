let cadena : string = "Algún Texto";
let numero : number = 25;
let decimal : number = 5.2;
let valorVerdad : boolean = true;
let anyVar : any = "Cadena";

const PHI : number = 3.1416;

let array1 : Array<number>;
let array2 : number[];

array1 = [1, 2, 3];
array2 = [4, 5];

console.log(cadena);
console.log(numero);
console.log(decimal);
console.log(valorVerdad);
console.log(cadena);

let soportaVarios : string | number;
soportaVarios = "Cadena";
console.log(soportaVarios);
soportaVarios = 1;
console.log(soportaVarios);

//let soportaUndefined : string | undefined;

type Persona = {
    name : string,
    age : number,
    birthDate : Date,
    empleado?: boolean
}

let p1 : Persona = {
    name : "Pedro",
    age: 30,
    birthDate: new Date()    
}

let p2 : Persona = {
    name : "Pedro",
    age: 30,
    birthDate: new Date(),
    empleado: false
}

class Animal{

    tipo : string;

    print(name : string) : void{
        console.log(name);
    }

}

let a : Animal = new Animal();
a.print("Name");

function sumar1(a : number, b : number) : number{
    return a + b;
}

function restar2(a : number, b : number) : void{
    console.log(a + b)
}